﻿using System;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            Console.Write("Укажите размер матрицы \nКоличество строк: ");

            int rows = Input();

            Console.Write("Количество столбцов: ");

            int columns = Input();

            int[,] array = new int [rows,columns];

            int sum = 0;

            int minSum = sum;

            int idMinSum = 0;

            Table.DrawHeader(columns);

            Console.WriteLine();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                sum = 0;

                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array[i, j] = random.Next(0,5);

                    sum += array[i, j];

                    Table.ShowMatrix(array, i, j);

                    if (i == 0)
                    {
                        minSum = sum;
                    }
                        
                }

                if (minSum > sum)
                {
                    minSum = sum;

                    idMinSum = i;
                }

                Console.Write("  " + sum);
                Console.WriteLine();

                if (i < rows - 1)
                {
                    Console.Write(Table.startBorder);

                    Table.DrawMiddle(columns);
                }
                else
                {
                    Table.DrawFooter(columns);
                }

                Console.WriteLine();
            }
            Console.WriteLine("Минимальная сумма элементов строки: " + minSum
                + ", ее мномер " + idMinSum);

            static int Input()
            {
                int result;

                while (!int.TryParse(Console.ReadLine(), out result))
                {
                    Console.Write("Ошибка ввода! Введите значение заново: ");
                }
                return result;
            }
            Console.ReadKey();
        }
    }
}
