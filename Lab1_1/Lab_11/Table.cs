﻿using System;

public static class Table
{
    public const string angleTopLeft = "\u250C";

    public const string angleTopRight = "\u2510";

    public const string angleBottomLeft = "\u2514";

    public const string angleBottomRight = "\u2518";

    public static string header = "\u2500\u2500\u252C";

    public const string startBorder = "\u251C";

    public static string middle = "\u2500\u2500\u253C";

    public static string footer = "\u2500\u2500\u2534";

    public static void DrawHeader(int columns)
    {
        Console.Write(angleTopLeft);

        for (int i = 0; i < columns; i++)
        {
            if (i == columns - 1)
            {
                Console.Write(header = header.Replace('\u252C', '\u2510'));
            }
            else
            {
                Console.Write(header);
            }
        }
    }

    public static void DrawMiddle(int columns)
    {
        for (int k = 0; k < columns; k++)
        {
            if (k == columns - 1)
            {
                Console.Write(middle = middle.Replace('\u253C', '\u2524'));
            }
            else
            {
                Console.Write(middle);
            }
        }
        middle = middle.Replace('\u2524', '\u253C');
    }

    public static void DrawFooter(int columns)
    {
        Console.Write(angleBottomLeft);

        for (int i= 0; i < columns; i++)
        {
            if (i == columns - 1)
            {
                Console.Write(footer = footer.Replace('\u2534', '\u2518'));
            }
            else
            {
                Console.Write(footer);
            }
        }
    }

    public static void ShowMatrix(int[,] array, int i, int j)
    {
        if (j == array.GetLength(1) - 1)
        {
            Console.Write("\u2502 " + array[i, j] + "\u2502");
        }
        else
        {
            Console.Write("\u2502 " + array[i, j]);
        }
    }
}
